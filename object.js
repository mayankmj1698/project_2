//Keys

function keys(obj) {
    let keyArr= [];
    for (var key in obj) {
        keyArr.push(key);
    }
    return keyArr;
}

//Values

function values(obj) {
    let valueArr= [];
    for (var key in obj) {
        valueArr.push(obj[key]);
    }
    return valueArr;
}


//mapObject

function mapObject(obj, cb) {
    let res = {};
    if( cb === undefined || obj === undefined){
        return null;
    }
    for (let key in obj){
        res[key] = cb(obj[key],key);
    }
  return res;
}


//Pairs

function pairs(obj) {
    let res = [];
    for (let key in obj){
        res.push([key,obj[key]]);
    }
    return res ;
}

//Invert

function invert(obj) {
    let res = {};
    for(let key in obj){
        res[obj[key]] = key ;
    }
    return res ;
}


// Defaults

function defaults(obj, defaultProps) {
    if (defaultProps === undefined || obj === undefined){
        return null ;
    }
    for (let key in defaultProps ){
        if (obj[key] === undefined){
            obj[key]=defaultProps[key];
        }
    }
    return obj ;
}




module.exports = {keys ,values, mapObject, pairs, invert ,defaults}