//each
function each(elements , cb) {
    if( cb === undefined || elements.length == 0){
        return [];
    }
    for (let i = 0; i < elements.length; i++){
        cb(elements[i]);
    }
    return cb;
}

//map
function map(elements , cb) {
    let newArr = [];
    if( cb === undefined || elements.length === 0){
        return [];
    }
    for (let i = 0; i < elements.length; i++){
        newArr.push(cb(elements[i]));
    }
   return newArr;
}

//reduce

function reduce(elements , cb, startvalue) {
    if( cb === undefined || elements.length === 0){
        return null;
    }

    if (!startvalue) {
        startvalue = elements[0]
        i = 1
      } else {
        i = 0
      }
      let reducevalue=startvalue;
    for ( i; i < elements.length; i++){
       reducevalue = cb(reducevalue,elements[i]);
    }
   return reducevalue;
  }

  //find
  function find(elements , cb) {
    if( cb === undefined || elements.length === 0){
        return null ;
    }
    for (let i = 0; i < elements.length; i++){
       if (cb(elements[i])){
           return elements[i] ;
       }
    }
    return undefined;
  }


  // filter

    function filter(elements, cb) {
    let newArr = [];
    if( cb === undefined || elements === undefined){
        return null;
    }
    for( let i = 0; i < elements.length; i++){
        if(cb(elements[i])){
            newArr.push(elements[i]) ;
        }
    }
    return newArr ;
  
  }

  //flatten
  
  function flatten(elements) {
      let newArr = [];
    if( elements === undefined || elements === null){
        return null;
    }
    for (let i = 0; i < elements.length; i++) {
        if (Number.isInteger(elements[i])) {
          newArr.push(elements[i])
        } else {
          let rec = this.flatten(elements[i])
          newArr.push(...rec)
        }
      }  
    return newArr;
  }

module.exports = { each,map,reduce,find,filter,flatten}