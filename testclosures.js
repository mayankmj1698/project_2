const closure = require('./closures')

//CounterFactory

//const counter = closure.counterFactory()
// console.log(counter.increment())
// console.log(counter.increment())
//console.log(counter.decrement())
//console.log(counter.decrement())

//limitFunctionCallCount
/*
const limit = closure.limitFunctionCallCount(() => {
   return 'done'
 }, 2)
 console.log(limit())
 console.log(limit())
 console.log(limit())
 console.log(limit())
 console.log(limit())*/

 //cacheFunction

const cachefunction = closure.cacheFunction(() => 'i will invoke')
console.log(cachefunction('ab'))
console.log(cachefunction('abc'))
console.log(cachefunction('abc'))
console.log(cachefunction('abcd'))
console.log(cachefunction('abcd'))