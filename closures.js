//Increment and Decrement

function counterFactory() {
    let count = 10

    return {
      increment() {
        count++
        return count
      },
      decrement() {
        count--
        return count
      }
    }
  }

  //LimitFunctionCallCount

  function limitFunctionCallCount(cb, n) {
    let times = 0

    return function () {
      if (times === n) 
      return null
      times++
      return cb()
    }
  }

  //cacheFunction

  function cacheFunction(cb) {
    let cache = {}

    return function (arg) {
      if (cache[arg]) 
      {
        return cache[arg]
      } 
      else {
        cache[arg] = 1
      }
      return cb()
    }
  }

  module.exports = { counterFactory , limitFunctionCallCount ,cacheFunction};