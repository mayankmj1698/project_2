const object = require('./object')
const obj = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }

// Keys

//const keyArr = object.keys(obj)
//console.log(keyArr)

// Values

//const valueArr = object.values(obj)
//console.log(valueArr)

//mapObject

//const obj2 = { start: 5, end: 10 }
//const mapObj = object.mapObject(obj2, function (val) {
 //return val + 5
 //})
//console.log(mapObj)

// Pairs

//const pair = object.pairs(obj)
//console.log(pair)

// Invert

//const invertObj = object.invert(obj)
//console.log(invertObj)

// Defaults

const defaultObj = object.defaults([1, 23, 4], {
   flavor: 'vanilla',
   sprinkles: 'lots',
 })
 console.log(defaultObj)
